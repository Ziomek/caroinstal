import { Parallax } from 'react-parallax';
import Para from './image/para2.jpg'

const ImagePar = () => {
    return (
        <Parallax strength={550} bgImage={Para}>
            <div className='image-par'></div>
        </Parallax>      
    )
};

export default ImagePar;