import {ReactComponent as Fb} from './image/fb.svg';

const Footer = () => {
    return (
        <div className="footer">
            <div className='footer-lok'>
                <h1>Lokalizacja</h1>
                <p>Usługi wykonujemy na terenie Krakowa oraz okolic.</p>
                <iframe className='footer-map ' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d327952.4182831036!2d19.724694226515123!3d50.0464284278981!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x471644c0354e18d1%3A0xb46bb6b576478abf!2zS3Jha8Ozdw!5e0!3m2!1sen!2spl!4v1650617597021!5m2!1sen!2spl" width="600" height="450" loading="lazy"></iframe>
            </div>
            <div className='footer-cont'>
                <h1>Kontakt</h1>
                <p>Karol Wołkowski</p>
                <a href="mailto:Car0instal@gmail.com">mail: Car0instal@gmail.com</a>
                <a href="tel:+48533403124">tel: 533403124</a>
            </div>
            {/* <a href='https://www.google.com/' target="_blank"> <Fb /></a> */}
        </div>
    );
};

export default Footer;