
import './App.css';
import ImagePar from './imagePar';
import Head from './Head';
import Main from './Main';
import Footer from './Footer';

function App() {
  return (
    <div className="App">
      <Head />
      <Main />
      <ImagePar/>
      <Footer />
    </div>
  );
}

export default App;
