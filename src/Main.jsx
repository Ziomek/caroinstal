import { useState } from 'react';
import WaterHeat from './image/water-heat.svg';
import AC from './image/ac.svg';
import Electrical from './image/electrical.svg';
import Heater from './image/heater.svg';
import Renovation from './image/renovation.svg';
import Pipes from './image/pipes.svg';
import Repairs from './image/repairs.svg';
import { Link } from "react-router-dom";
import Podlogowe1 from './image/podlogowe1.jpg';
import Podlogowe2 from './image/podlogowe2.jpg';

const Main = () => {
    const [isVisible, setVisibility] = useState(false);
    const [desc, setDesc] = useState("");
    const wodne = `Montaż instalacji wodno-kanalizacyjnych
    Wykonujemy instalacje wewnętrzne:
    instalacje kanalizacyjne (PCV 
    instalacje wodne,
    Instalację wodną tworzymy zazwyczaj PEX.
    Po każdej zrobionej istalacji są wykonywane próby szczelności`;
    const elek = `Wykonujemy instalacje wewnętrzne:

    Wszelkiego rodzaju instalacje elektryczne
    
    Instalacje odgromowe
    
    Remonty starych instalacji elektrycznych
    
    Montaż kuchenek elektrycznych,
    
    ceramicznych, indukcyjnych oraz piekarników`;
    const cieplne = <div><p>
    Ogrzewanie podłogowe wodne
    zapewnia w Twoim domu równomierne ogrzewanie całego pomieszczenia i zmniejszy koszty związane z użyciem energii. Jeśli chcesz mieć założone ogrzewanie podłogowe zapoznaj się z usługą montażu instalacji wodnej lub elektrycznej, która podgrzeje Twoją podłogę.
   Pionowy rozkład temperatury wpływa korzystnie na Twój organizm. Ogrzewa ona powietrze znajdujące się zaraz przy podłodze, dzięki czemu ciepłe powietrze znajduje się bliżej stóp, a chłodniejsze powietrze unosi się w górę w kierunku głowy. Pamiętaj jednak, że to nie podłoga jest ciepła tylko powietrze unoszące się nad nią. Instalacje c.o.
   Jednym z najczęściej wybieranych przez klientów sposobów na ogrzewanie domu jest instalacja centralnego ogrzewania. Jest to kocioł z systemem rur i grzejników, które rozprowadzają ciepło po wszystkich pomieszczeniach za pomocą przepływającej rurami gorącej wody. Jeśli masz zamontowany piecyk gazowy lub grzejnik elektryczny ogrzejesz tylko pomieszczenie, w którym on się znajduje. Jeśli zdecydujesz się zainstalować centralne ogrzewanie będziesz mógł ogrzać wszystkie pomieszczenia, w których zamontowana jest instalacja.
   </p>
   <img className='main-img' src={Podlogowe1} />
   <img className='main-img' src={Podlogowe2} />
   </div>;
    const clickHandle = (text) => {
        setVisibility(true);
        setDesc(text);
    };

    return (
        <div className='main_css'>
            <h1>Nasze usługi</h1>
            <div className='main-wrapper'>
                <div className='sweep-wrapper-fore' onClick={() => clickHandle(wodne)}>
                    <img className='icon-css' src={WaterHeat}/>
                    <p>Instalacje wodno - kanalizacyjne</p>
                </div>
                <div className='sweep-wrapper-fore' onClick={() => clickHandle(elek)}>
                    <img className='icon-css' src={Electrical}/>
                    <p>Instalacje elektryczne</p>
                </div>
                <div className='sweep-wrapper-fore' onClick={() => clickHandle(cieplne)}>
                    <img className='icon-css' src={Heater}/>
                    <p>Instalacje grzewcze</p>
                </div>
            </div>
            <div>
                {isVisible && <p>{desc}</p>}
            </div>          
        </div>
    );
};

export default Main;