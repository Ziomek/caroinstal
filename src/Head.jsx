import head  from './image/head.jpg';
import Key from './image/key.svg';

const Head = () => {
    return (
        <div className='header-main'>
            <div className='head_css'>
                <img className='head_css_img' src={head} />
            </div>
            <div className='head-title-wrapper'>
                <h1 className='header_title'>CaroInstal</h1>
                <img className='head-svg' src={Key} />
            </div>
            <div className='head-desc'>
                <p> Firma remontowo - budowlana </p>
            </div>         
        </div>
        
    );
}

export default Head;