import Xml from './sitemap.xml';
import React from "react";
import ReactDOM from "react-dom";
import XMLViewer from "react-xml-viewer";

const Sitemap = () => {

    const xml =
    `<?xml version="1.0" encoding="UTF-8"?>
    <urlset
          xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
                http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    
    
    <url>
      <loc>https://www.caroinstal.pl/</loc>
      <lastmod>2022-04-27T19:09:33+00:00</lastmod>
    </url>
    
    
    </urlset>`;

    return (
        <div >
            <XMLViewer xml={xml} />
        </div>
    );
};

export default Sitemap;